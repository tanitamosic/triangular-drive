package com.NWT_KTS_project.model.enums;

public enum RideStatus {
    RESERVED,
    PENDING,
    ONGOING,
    FINISHED,
    REJECTED,
    EMERGENCY
}
