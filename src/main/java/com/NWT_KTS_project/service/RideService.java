package com.NWT_KTS_project.service;

import com.NWT_KTS_project.model.Address;
import com.NWT_KTS_project.model.Reservation;
import com.NWT_KTS_project.model.Ride;
import com.NWT_KTS_project.model.Route;
import com.NWT_KTS_project.model.enums.DriverStatus;
import com.NWT_KTS_project.model.enums.RideStatus;
import com.NWT_KTS_project.model.users.Client;
import com.NWT_KTS_project.model.users.Driver;
import com.NWT_KTS_project.repository.AddressRepository;
import com.NWT_KTS_project.repository.ReservationRepository;
import com.NWT_KTS_project.repository.RideRepository;
import com.NWT_KTS_project.repository.RouteRepository;
import com.NWT_KTS_project.util.comparators.ride.RideComparator;
import com.NWT_KTS_project.util.comparators.ride.RideDateComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class RideService{

    @Autowired
    private RideRepository rideRepository;

    @Autowired
    RouteRepository routeRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private DriverService driverService;

    @Autowired
    private ReservationRepository reservationRepository;

    public Ride getRideById(int id){
        return rideRepository.findByRideId(id);
    }

    public List<Ride> getRidesByUserId(int id, RideComparator comparator) {
        List<Ride> rides = rideRepository.findByPassengersId(id);
        rides.addAll(rideRepository.findByDriverId(id));
        if (comparator != null) {
            rides.sort(comparator);
        }
        return rides;
    }

    public List<Ride> getAllFinishedRides() {
        return rideRepository.getAllByStatusOrderByDepartureTimeDesc(RideStatus.FINISHED);
    }


    public void markRideAsFinished(int id, RideStatus status){
        Ride ride = rideRepository.findById(id).get();
        ride.setArrivalTime(LocalDateTime.now());
        ride.setStatus(status);
        rideRepository.saveAndFlush(ride);
    }

    public void setRideStatus(int id, RideStatus status){
        Ride ride = rideRepository.findById(id).get();
        ride.setStatus(status);
        rideRepository.saveAndFlush(ride);
    }


    public Ride getNextRideForDriver(int id){
        List<Ride> rides = rideRepository.findByDriverId(id);
        ArrayList<Ride> pendingRides = new ArrayList<Ride>();

        for (Ride ride : rides) {
            if (ride.getStatus() == RideStatus.PENDING) {
                pendingRides.add(ride);
            }
        }
        pendingRides.sort(new RideDateComparator());

        if(pendingRides.size()<=1){
            driverService.setDriverStatus(id, DriverStatus.AVAILABLE);
        }

        if (pendingRides.size() > 0) {
            return pendingRides.get(0);
        }
        return null;
    }


    public Ride createRide(Driver driver, List<Address> addresses,
                           List<Client> clients, RideStatus status, String price){
        for (Address address : addresses) {
            addressRepository.saveAndFlush(address);
        }
        Ride ride = new Ride();
        ride.setDriver(driver);
        ride.setPassengers(clients);
        Route route = new Route();
        route.setStart(addresses.get(0));
        route.setDestination(addresses.get(addresses.size()-1));
        ArrayList<Address> stops = new ArrayList<Address>();
        for(int i = 1; i < addresses.size()-1; i++){
            stops.add(addresses.get(i));
        }
        ride.setDepartureTime(LocalDateTime.now());
        route.setStops(stops);
        ride.setRoute(route);
        ride.setStatus(status);
        ride.setPrice(Double.parseDouble(price));
        routeRepository.saveAndFlush(route);
        rideRepository.saveAndFlush(ride);
        return ride;
    }

    public Reservation makeReservation(Driver driver, List<Address> addresses,
                                       List<Client> clients, RideStatus status,
                                       LocalDateTime time, String price){
        Ride ride = createRide(driver,addresses,clients,status,price);
        Reservation res = new Reservation();
        res.setRide(ride);
        res.setTime(time);
        ride.setDepartureTime(time);
        reservationRepository.saveAndFlush(res);
        return res;
    }

    public List<Ride> getAssignedRide(Integer id) {
        return rideRepository.getPendingRide(id);
    }

    public List<Ride> getDriverReservedRide(Integer id) {
        List<Reservation> res = reservationRepository.findByDriver(id);
        List<Ride> rides = new ArrayList<Ride>();
        for(Reservation r : res){
            long minutes =LocalDateTime.now().until(r.getTime(), ChronoUnit.MINUTES);
            if(r.getRide().getDriver().getId()==id && minutes<=5){
                rides.add(r.getRide());
            }
        }
        return rides;
    }

    public Long getTimeUntilReservation(Integer id) {
        List<Reservation> res = reservationRepository.findByPassenger(id);
        List<Ride> rides = new ArrayList<Ride>();
        for(Reservation r : res){
            long minutes =LocalDateTime.now().until(r.getTime(), ChronoUnit.MINUTES);
            if(r.getRide().getDriver().getId()==id && minutes<=15){
                return minutes;
            }
        }
        return -1L;
    }

    public List<Ride> getRides(LocalDateTime dateTime1, LocalDateTime dateTime2) {
        // ADMIN
        return rideRepository.getRides(dateTime1, dateTime2);
    }
    public List<Ride> getDriverRides(Integer driverId, LocalDateTime dateTime1, LocalDateTime dateTime2) {
        // DRIVER
        return rideRepository.getDriverRides(driverId, dateTime1, dateTime2);
    }
    public List<Ride> getClientRides(Integer clientId, LocalDateTime dateTime1, LocalDateTime dateTime2) {
        return rideRepository.getClientRides(clientId, dateTime1, dateTime2);
    }
    public void saveRide(Ride ride){
        rideRepository.saveAndFlush(ride);
    }
}
